 # -*- coding: utf-8 -*-
"""
Elisa Castro	587303
Letícia Berto	587354
"""
#imports
from threading import Thread
import time
#999 = infito
#a ordem dos nós tanto nas linhas quanto nas colunas é: 0, 1, 2, 3

def imprimeMatriz(matriz):
	for pos_linha in range(len(matriz)):
		for pos_coluna in range(len(matriz[0])):
			print "%3d" % matriz[pos_linha][pos_coluna],
		print

#Inicializa a tabela de cada nó
def rtinit0():
	global no0
	no0 = []
	for i in range(4):
		no0.append([999] * 4)
	no0[0][0] = 0
	no0[0][1] = 1
	no0[0][2] = 3
	no0[0][3] = 7
	"""
	print "Tabela Nó 0:"
	imprimeMatriz(no0)
	"""
	time.sleep(1)
	rt1 = Thread(target=rtupdate1, args=(0, no0[0][0:4]))
	rt2 = Thread(target=rtupdate2, args=(0, no0[0][0:4]))
	rt3 = Thread(target=rtupdate3, args=(0, no0[0][0:4]))
	rt1.start()
	rt2.start()
	rt3.start()
	rt1.join()
	rt2.join()
	rt3.join()
	"""rtupdate1(0, no0[0][0:4])
				rtupdate2(0, no0[0][0:4])
				rtupdate3(0, no0[0][0:4])"""

def rtinit1():
	global no1
	no1 = []
	for i in range(4):
		no1.append([999] * 4)
	no1[1][0] = 1
	no1[1][1] = 0
	no1[1][2] = 1
	"""
	print "Tabela Nó 1:"
	imprimeMatriz(no1)
	"""
	time.sleep(1)
	rt0 = Thread(target=rtupdate0, args=(1, no1[1][0:4]))
	rt2 = Thread(target=rtupdate2, args=(1, no1[1][0:4]))
	rt3 = Thread(target=rtupdate3, args=(1, no1[1][0:4]))
	rt0.start()
	rt2.start()
	rt3.start()
	rt0.join()
	rt2.join()
	rt3.join()
	"""rtupdate0(1, no1[1][0:4])
				rtupdate2(1, no1[1][0:4])
				rtupdate3(1, no1[1][0:4])"""

def rtinit2():
	global no2
	no2 = []
	for i in range(4):
		no2.append([999] * 4)
	no2[2][0] = 3
	no2[2][1] = 1
	no2[2][2] = 0
	no2[2][3] = 2
	"""
	print "Tabela Nó 2:"
	imprimeMatriz(no2)
	"""
	time.sleep(1)
	rt0 = Thread(target=rtupdate0, args=(2, no2[2][0:4]))
	rt1 = Thread(target=rtupdate1, args=(2, no2[2][0:4]))
	rt3 = Thread(target=rtupdate3, args=(2, no2[2][0:4]))
	rt0.start()
	rt1.start()
	rt3.start()
	rt0.join()
	rt1.join()
	rt3.join()
	"""rtupdate0(2, no2[2][0:4])
				rtupdate1(2, no2[2][0:4])
				rtupdate3(2, no2[2][0:4])"""


def rtinit3():
	global no3
	no3 = []
	for i in range(4):
		no3.append([999] * 4)
	no3[3][0] = 7
	no3[3][2] = 2
	no3[3][3] = 0
	"""
	print "Tabela Nó 3:"
	imprimeMatriz(no3)
	"""
	time.sleep(1)
	rt0 = Thread(target=rtupdate0, args=(3, no3[3][0:4]))
	rt1 = Thread(target=rtupdate1, args=(3, no3[3][0:4]))
	rt2 = Thread(target=rtupdate2, args=(3, no3[3][0:4]))
	rt0.start()
	rt1.start()
	rt2.start()
	rt0.join()
	rt1.join()
	rt2.join()
	"""rtupdate0(3, no3[3][0:4])
				rtupdate2(3, no3[3][0:4])"""


#Dx(z) = min{c(x,y) + Dy(z), c(x,z) + Dz(z)} = min{2+1 , 7+0} = 3
def rtupdate0(no,matriz): #recebe somente a linha referente a do próprio nó que enviou e a indicação de qual nó foi
	for pos_coluna in range(len(matriz)):
		no0[no][pos_coluna] = matriz[pos_coluna]
	#refaz o calculo referente ao seu proprio nó depois de receber atualização dos demais nós e se alterar avisa os outros
	distNo01Anterior = no0[0][1] #armazena o valor anterior para dps ver se teve atualização
	distNo02Anterior = no0[0][2]
	distNo03Anterior = no0[0][3]
	time.sleep(1)
	#atualizar distancia para nó 1
	no0[0][1] = min(no0[0][1] + no0[1][1], no0[0][2] + no0[2][1], no0[0][3] + no0[3][1])
	#atualizar distancia para nó 2
	no0[0][2] = min(no0[0][2] + no0[2][2], no0[0][1] + no0[1][2], no0[0][3] + no0[3][2])
	#atualizar distancia para nó 3
	no0[0][3] = min(no0[0][3] + no0[3][3], no0[0][1] + no0[1][3], no0[0][2] + no0[2][3])
	time.sleep(1)
	if distNo01Anterior != no0[0][1] or distNo02Anterior != no0[0][2] or distNo03Anterior != no0[0][3]:
		rt1 = Thread(target=rtupdate1, args=(0, no0[0][0:4]))
		rt2 = Thread(target=rtupdate2, args=(0, no0[0][0:4]))
		rt3 = Thread(target=rtupdate3, args=(0, no0[0][0:4]))
		rt1.start()
		rt2.start()
		rt3.start()
		rt1.join()
		rt2.join()
		rt3.join()
		"""rtupdate1(0, no0[0][0:4])
								rtupdate2(0, no0[0][0:4])
								rtupdate3(0, no0[0][0:4])"""
	#imprimeMatriz(no0)

def rtupdate1(no,matriz): #recebe somente a linha referente a do próprio nó que enviou e a indicação de qual nó foi
	for pos_coluna in range(len(matriz)):
		no1[no][pos_coluna] = matriz[pos_coluna]
	#refaz o calculo referente ao seu proprio nó depois de receber atualização dos demais nós e se alterar avisa os outros
	distNo10Anterior = no1[1][0] #armazena o valor anterior para dps ver se teve atualização
	distNo12Anterior = no1[1][2]
	distNo13Anterior = no1[1][3]
	time.sleep(1)
	#atualizar distancia para nó 0
	no1[1][0] = min(no1[1][0] + no1[0][0], no1[1][2] + no1[2][0], no1[1][3] + no1[3][0])
	#atualizar distancia para nó 2
	no1[1][2] = min(no1[1][2] + no1[2][2], no1[1][0] + no1[0][2], no1[1][3] + no1[3][2])
	#atualizar distancia para nó 3
	no1[1][3] = min(no1[1][3] + no1[3][3], no1[1][0] + no1[0][3], no1[1][2] + no1[2][3])
	time.sleep(1)
	if distNo10Anterior != no1[1][0] or distNo12Anterior != no1[1][2] or distNo13Anterior != no1[1][3]:
		rt0 = Thread(target=rtupdate0, args=(1, no1[1][0:4]))
		rt2 = Thread(target=rtupdate2, args=(1, no1[1][0:4]))
		rt3 = Thread(target=rtupdate3, args=(1, no1[1][0:4]))
		rt0.start()
		rt2.start()
		rt3.start()
		rt0.join()
		rt2.join()
		rt3.join()
		"""rtupdate0(1, no1[1][0:4])
								rtupdate2(1, no1[1][0:4])
								rtupdate3(1, no1[1][0:4])"""
		#nao manda para o no3 porque nao esta diretamente ligado a ele
	#imprimeMatriz(no1)

def rtupdate2(no,matriz): #recebe somente a linha referente a do próprio nó que enviou e a indicação de qual nó foi
	for pos_coluna in range(len(matriz)):
		no2[no][pos_coluna] = matriz[pos_coluna]
	#refaz o calculo referente ao seu proprio nó depois de receber atualização dos demais nós e se alterar avisa os outros
	distNo20Anterior = no2[2][0] #armazena o valor anterior para dps ver se teve atualização
	distNo21Anterior = no2[2][1]
	distNo23Anterior = no2[2][3]
	time.sleep(1)
	#atualizar distancia para nó 0
	no2[2][0] = min(no2[2][0] + no2[0][0], no2[2][1] + no2[1][0], no2[2][3] + no2[3][0])
	#atualizar distancia para nó 1
	no2[2][1] = min(no2[2][1] + no2[1][1], no2[2][3] + no2[3][1], no2[2][0] + no2[0][1])
	#atualizar distancia para nó 3
	no2[2][3] = min(no2[2][3] + no2[3][3], no2[2][1] + no2[1][3], no2[2][0] + no2[0][3])
	time.sleep(1)
	if distNo20Anterior != no2[2][0] or distNo21Anterior != no2[2][1] or distNo23Anterior != no2[2][3]:
		rt0 = Thread(target=rtupdate0, args=(2, no2[2][0:4]))
		rt1 = Thread(target=rtupdate1, args=(2, no2[2][0:4]))
		rt3 = Thread(target=rtupdate3, args=(2, no2[2][0:4]))
		rt0.start()
		rt1.start()
		rt3.start()
		rt0.join()
		rt1.join()
		rt3.join()
		"""rtupdate0(2, no2[2][0:4])
								rtupdate1(2, no2[2][0:4])
								rtupdate3(2, no2[2][0:4])"""
	#imprimeMatriz(no2)

def rtupdate3(no,matriz): #recebe somente a linha referente a do próprio nó que enviou e a indicação de qual nó foi
	for pos_coluna in range(len(matriz)):
		no3[no][pos_coluna] = matriz[pos_coluna]
	#refaz o calculo referente ao seu proprio nó depois de receber atualização dos demais nós e se alterar avisa os outros
	distNo30Anterior = no3[2][0] #armazena o valor anterior para dps ver se teve atualização
	distNo31Anterior = no3[2][1]
	distNo32Anterior = no3[2][3]
	time.sleep(1)
	#atualizar distancia para nó 0
	no3[3][0] = min(no3[3][0] + no3[0][0], no3[3][2] + no3[2][0], no3[3][1] + no3[1][0])
	#atualizar distancia para nó 1
	no3[3][1] = min(no3[3][1] + no3[1][1], no3[3][2] + no3[2][1], no3[3][0] + no3[0][1])
	#atualizar distancia para nó 2
	no3[3][2] = min(no3[3][2] + no3[2][2], no3[3][1] + no3[1][2], no3[3][0] + no3[0][2])
	time.sleep(1)
	if distNo30Anterior != no3[3][0] or distNo31Anterior != no3[3][1] or distNo32Anterior != no3[3][2]:
		rt0 = Thread(target=rtupdate0, args=(3, no3[3][0:4]))
		rt1 = Thread(target=rtupdate1, args=(3, no3[3][0:4]))
		rt2 = Thread(target=rtupdate2, args=(3, no3[3][0:4]))
		rt0.start()
		rt1.start()
		rt2.start()
		rt0.join()
		rt1.join()
		rt2.join()
		"""rtupdate0(3, no3[3][0:4])
								rtupdate2(3, no3[3][0:4])
								rtupdate1(3, no3[3][0:4])"""
		#nao manda para o no1 porque nao esta diretamente ligado a ele
	#imprimeMatriz(no3)
#Dx(y)
#def Dxy(x, y):

if __name__== "__main__":
	try:
		zero = Thread(target=rtinit0, args=())
		um = Thread(target=rtinit1, args=())
		dois = Thread(target=rtinit2, args=())
		tres = Thread(target=rtinit3, args=())
		zero.start()
		um.start()
		dois.start()
		tres.start()
	except:
		print("Falha ao abrir as threads.")

	#gambis pra parar
	"""fim = raw_input("Para sair aperte f")
				if fim == "f":"""
	zero.join()
	um.join()
	dois.join()
	tres.join()
	print "Matriz 0:"
	imprimeMatriz(no0)

	print "Matriz 1:"
	imprimeMatriz(no1)

	print "Matriz 2:"
	imprimeMatriz(no2)

	print "Matriz 3:"
	imprimeMatriz(no3)
